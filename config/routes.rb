Rails.application.routes.draw do
  devise_for :admins, path: 'publisher', path_names: { sign_in: '', sign_out: 'logout'}

  ## PUBLISHER
  get '/publisher/inscricoes' => 'publisher/users#index', as: :publisher_users
  get '/publisher/inscricoes/:id/barcode' => 'publisher/users#barcode', as: :barcode_publisher_users
  get '/publisher/inscricoes/busca' => 'publisher/users#search', as: :search_publisher_users
  get '/publisher/inscricoes/filtros' => 'publisher/users#filters', as: :filter_publisher_users
  post   '/publisher/inscricoes/:id/confirm-attended' => 'publisher/users#confirm_attended'
  delete '/publisher/inscricoes/:id' => 'publisher/users#destroy', as: :publisher_user
  
  get '/publisher/noticias' => 'publisher/news#index', as: :news_index
  get '/publisher/noticias/nova' => 'publisher/news#new', as: :new_publisher_news
  post '/publisher/noticias' => 'publisher/news#create', as: :publisher_news
  get '/publisher/noticias/:id/editar' => 'publisher/news#edit', as: :edit_publisher_news
  patch '/publisher/noticias/:id' => 'publisher/news#update', as: :update_publisher_news

  # USERS
  get '/inscricoes' => 'users#new', as: :new_user
  get '/inscricoes/:unique_identifier/confirm-attend' => 'users#confirm_attend'
  post '/users' => 'users#create'

  ## NEWS
  get '/noticias' => 'news#index', as: :news

  post '/visitors/contacts' => 'visitors#contacts', as: :contact_visitors

  get '/,' => 'visitors#index' # fixinig fucking error on mailer 
  root to: 'visitors#index'

  # CERTIFICATE
  get '/certificado(/:name)' => 'visitors#certificate'

  get '/:partial_name' => 'sandbox#index', :partial_name => /.*/
end
