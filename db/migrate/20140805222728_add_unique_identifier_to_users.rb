class AddUniqueIdentifierToUsers < ActiveRecord::Migration
  def change
    add_column :users, :unique_identifier, :string
  end
end
