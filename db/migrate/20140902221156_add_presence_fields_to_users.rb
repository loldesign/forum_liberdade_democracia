class AddPresenceFieldsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :want_to_attend, :boolean, default: false
    add_column :users, :attended      , :boolean, default: false
  end
end