class AddNewsletterToUsers < ActiveRecord::Migration
  def change
    add_column :users, :newsletters, :boolean, default: false
  end
end
