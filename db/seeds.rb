# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin = Admin.create!(
  email: 'admin@forumliberdadedemocracia.com.br',
  password: 'adminforumsp2014',
  password_confirmation: 'adminforumsp2014'
)

puts 'CREATED ADMIN USER: ' << admin.email
