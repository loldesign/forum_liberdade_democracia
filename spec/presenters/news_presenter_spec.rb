require 'spec_helper'

describe NewsPresenter do
  describe '#side' do
    let(:presenter) {described_class.new(build(:news))}

    context 'odd' do
      it {expect(presenter.side(0)).to eq('left')}
    end

    context 'even' do
      it {expect(presenter.side(1)).to eq('right')}
    end
  end

  describe '#external_link' do
    let(:presenter) {described_class.new(build(:news, link: link))}

    context 'with scheme' do
      let(:link) {'www.loldesign.com.br'}
      it {expect(presenter.external_link).to eq('http://www.loldesign.com.br')}
    end

    context 'without scheme' do
      let(:link) {'https://rubygems.org'}
      it {expect(presenter.external_link).to eq('https://rubygems.org')}
    end
  end
end
