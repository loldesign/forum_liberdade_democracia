# == Schema Information
#
# Table name: news
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  content    :string(255)
#  link       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'rails_helper'

describe News do
  it {should validate_presence_of :title}
  it {should validate_presence_of :link }
end
