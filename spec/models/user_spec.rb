# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  email             :string(255)      default(""), not null
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  company           :string(255)
#  newsletters       :boolean          default(FALSE)
#  phone             :string(255)
#  unique_identifier :string(255)
#

describe User do
  it {should validate_presence_of :name}
  it {should validate_presence_of :email}

  it {should validate_uniqueness_of :email}

  describe 'validate email' do
    context 'valid' do
      let(:user) {build(:user, email: 'rocky@rockyland.com')}
      it {expect(user).to be_valid}
    end

    context 'invalid' do
      let(:user) {build(:user, email: 'wrong-email')}
      it {expect(user).to_not be_valid}
      it {expect(user).to have(1).errors_on(:email)}
    end
  end
end
