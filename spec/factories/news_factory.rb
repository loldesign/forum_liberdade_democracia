# == Schema Information
#
# Table name: news
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  content    :string(255)
#  link       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :news do
    title {Faker::Lorem.sentence}
    content {Faker::Lorem.paragraph(2)}
    link {Faker::Internet.url}
  end
end
