# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  email             :string(255)      default(""), not null
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  company           :string(255)
#  newsletters       :boolean          default(FALSE)
#  phone             :string(255)
#  unique_identifier :string(255)
#

FactoryGirl.define do
  factory :user do
    name {Faker::Name.name}
    email {Faker::Internet.email}

    company {Faker::Company.name}
  end
end
