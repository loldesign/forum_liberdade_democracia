namespace :notyfications do
  desc "Send email to customers asking to confirm attend on event"
  task confirm_attend: :environment do
    User.all.each_slice(500).each_with_index do |users, index|
      puts "#{index} list processing -------------\n"
      
      send_email_to(list: users, 
                    title: 'Confirme sua presença no 1º Fórum Liberdade e Democracia SP', 
                    action_path: Rails.root.join('app','views', 'notyfications', 'confirm_attend.html'))

      wait_a_bit
    end
  end

  desc "Remember all users thats event is tomorrow"
  task remember_event: :environment do
    User.all.each_slice(500).each_with_index do |users, index|
      puts "#{index} list processing -------------\n"
      
      send_email_to(list: users, 
                    title: 'É Amanhã: 1º Fórum Liberdade e Democracia SP', 
                    action_path: Rails.root.join('app','views', 'notyfications', 'remember_event.html'))
    
      wait_a_bit
    end
  end

  desc "TODO"
  task send_certificate: :environment do
    people_list = read_csv_file('lista_certificados_forum2.csv')

    send_email_to(list: people_list,
                  title: 'Certificado do 1º Fórum Liberdade e Democracia SP',
                  fields: [:name],
                  action_path: Rails.root.join('app','views', 'notyfications', 'certification.html')
                 )
  end

end

def get_file_as_string(filename)
  data = ''
  f = File.open(filename, "r") 
  f.each_line do |line|
    data += line
  end
  return data
end

def send_email_to options={}
  recipients = create_recipients(list: options[:list], fields: options[:fields] || [:name, :unique_identifier])

  puts recipients

  send_email(title:         options[:title],
             recipients:    recipients,
             template_html: get_file_as_string(options[:action_path])
            )
end

def send_email(options={})
  PostageApp::Request.new(:send_message,
    :message => {
      'text/html'  => options[:template_html]
    },
    :recipients => options[:recipients],
    :headers    => {
      'Subject' => options[:title],
      'From'    => 'inscricoes@iflsp.org'
    }
  ).send
end

def wait_a_bit
  time_to_wait = rand(5) * 60

  puts "will wait #{time_to_wait / 60} minutes... -------------\n"
  
  sleep(time_to_wait)
end

def create_recipients(options={})
  recipients = {}
  
  options[:list].each do |user|
     recipients = recipients.merge(user.email => options[:fields].collect{|field| [field, user.send(field)]}.to_h)
  end

  recipients
end


def read_csv_file(file)
  require 'csv'

  people = []

  CSV.foreach(File.path(Rails.root.join('lib', 'tasks', file))) do |col|
      data = col[0].split(';')

      name, email = data[0] , data[1]

      people << OpenStruct.new(name: name, email: email)
  end

  people.reject{|x| x.email.nil?}.collect{|x| x}
end
# -----------------
# HOW TO USE
# -----------------
# user = User.find(1652)

# send_email(title:         'Confirme sua presença no 1º Fórum Liberdade e Democracia SP',
#            recipients:    {user.email => {name: user.name, unique_identifier: user.unique_identifier}},
#            template_html: get_file_as_string(Rails.root.join('app','views', 'notyfications', 'confirm_attend.html'))
#           )