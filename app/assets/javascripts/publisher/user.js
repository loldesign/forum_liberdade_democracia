jQuery(document).ready(function($) {
  new checkboxUserManager();
});

var checkboxUserManager = function(){
  var _this = this;
  this.checkboxes = $("[name='user[attended]']");

  this.startup = function(){
    if(!this.checkboxes[0]){ return false; }

    this.checkboxes.change(function(event) {
      
      var $field = $(this);

      _this.confirmUserAttended({id:        $field.data('id'),
                                 isChecked: $field.is(':checked')});
    });
  },

  this.confirmUserAttended = function(object){
    console.log(object.id, object.isChecked);
    $.ajax({
      url: '/publisher/inscricoes/'+object.id+'/confirm-attended',
      type: 'POST',
      dataType: 'html',
      data: {is_checked: object.isChecked},
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }

  this.startup();
}