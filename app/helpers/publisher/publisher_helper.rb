module Publisher::PublisherHelper
  def active?(resource)
    'active' if request.path.match(resource)
  end

  def barcode(user)
    image_tag("barcode/#{user.id}/code128b.png")
  end
end
