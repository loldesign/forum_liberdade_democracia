class ContactMailer < ActionMailer::Base
  def send_message(options={})
    load_attachments()
        
    @name      = options[:name]
    @telephone = options[:telephone]
    @email     = options[:email]
    @message   = options[:message]

    mail(:to      => "inscricoes@iflsp.org",
         :from    => @email,
         :subject => "E-mail de Contato do site Forum Liberdade e Democracia SP")
  end

  def send_user_confirmation(user)
    load_attachments()

    @user = user

    mail(:to      => @user.email,
         :from    => "inscricoes@iflsp.org",
         :subject => "Inscrição no 1º Fórum Liberdade e Democracia SP")
  end

  private
  def load_attachments
    attachments.inline['mail-logo.png']        = File.read(Rails.root.join('app/assets/images', 'mail-logo.png'))
    attachments.inline['mail-brasil-logo.png'] = File.read(Rails.root.join('app/assets/images', 'mail-brasil-logo.png'))
  end
end
