# == Schema Information
#
# Table name: news
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  content    :string(255)
#  link       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class News < ActiveRecord::Base
  validates :title, :link, presence: true

  default_scope { order('created_at DESC') }
end
