# == Schema Information
#
# Table name: users
#
#  id                :integer          not null, primary key
#  email             :string(255)      default(""), not null
#  created_at        :datetime
#  updated_at        :datetime
#  name              :string(255)
#  company           :string(255)
#  newsletters       :boolean          default(FALSE)
#  phone             :string(255)
#  unique_identifier :string(255)
#

class User < ActiveRecord::Base
  validates :name, :email, :company, presence: true
  validates :email, uniqueness: true
  validates :email, email: true

  scope :filter_by_param, -> (param) {where('users.company ILIKE ? OR users.name ILIKE ? OR users.email ILIKE ?', "%#{param}%", "%#{param}%", "%#{param}%")}

  scope :last_created, -> { order('created_at DESC') }

  before_create :generate_unique_identifier

  def barcode_file
    require 'barby/barcode/code_128'
    require 'barby/outputter/png_outputter'

    Barby::Code128B.new(self.unique_identifier).to_png(height: 50, margin: 5)
  end

  def self.to_csv(users)
    require 'csv'

    CSV.generate(options={}) do |csv|
      csv << ['Name', 'Email', 'Company', 'Pretende Comparecer?' ,'Id']

      users.each do |user|
        csv << [user.name, user.email, user.company, I18n.t(user.want_to_attend.to_s, scope: [:want_to_attend]), user.unique_identifier]
      end
    end
  end

  private

  def generate_unique_identifier
    require 'securerandom'

    self.unique_identifier = SecureRandom.uuid
  end
end
