class Publisher::NewsController < Publisher::PublisherController
  defaults resource_class: News
  actions :index, :new, :create, :edit, :update, :destroy

  def permitted_params
    params.permit(news: [:title, :link, :content])
  end

  def create
    create! do |success, _|
      success.html { redirect_to news_index_path, notice: 'Notícia criada com sucesso.' }
    end
  end

  def update
    update! do |success, _|
      success.html { redirect_to news_index_path, notice: 'Notícia editada com sucesso.' }
    end
  end

  protected

  def collection
    @news ||= end_of_association_chain.order('created_at DESC').page params[:page]
  end
end
