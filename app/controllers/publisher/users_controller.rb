require 'iconv'

class Publisher::UsersController < Publisher::PublisherController
  defaults resource_class: User
  actions :index, :show, :except => [:search, :barcode, :filters]

  def index
    
    @users = User.where(created_at: [Date.today.at_beginning_of_year..Date.today.at_end_of_day]).filter_by_param(params[:query]).page params[:page]

    render action: :index
  end


  def search

    @users = User.where(created_at: [Date.today.at_beginning_of_year..Date.today.at_end_of_day]).filter_by_param(params[:query]).page params[:page]

    render action: :index
  end

  def filters
    @users = UserFiltersManager.new(params).filter

    respond_to do |format|
      format.html do
        @users = @users.page(params[:page] || 1)
        render action: :index
      end

      format.csv do
        content = User.to_csv(@users)
        content = Iconv.conv("iso-8859-1//IGNORE", "utf-8", content) 
        send_data content
      end
    end

  end

  def barcode
    @user = User.find params[:id]

    send_data(@user.barcode_file, filename: "#{@user.unique_identifier}.png", type: 'image/png')
  end

  def destroy
    @user = User.find params[:id]

    @user.destroy

    redirect_to publisher_users_path, notice: 'Usuário removido com sucesso.'
  end

  def confirm_attended
    @user = User.find params[:id]

    if @user.update_attribute(:attended, params[:is_checked])
      head :ok
    else
      head :error
    end
  end

  protected

  def collection
    @users ||= end_of_association_chain.last_created.page params[:page]
  end
end
