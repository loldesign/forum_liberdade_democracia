class Publisher::PublisherController < InheritedResources::Base
  layout 'publisher'
  respond_to :html, :json, :csv

  before_action :authenticate_admin!
end
