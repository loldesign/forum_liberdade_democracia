class VisitorsController < ApplicationController
  def index
    @news = News.limit(5) 
  end

  def contacts
    ContactMailer.send_message(params).deliver

    redirect_to '/contato', notice: 'Sua mensagem de contato foi enviada com sucesso.'
  end

  def certificate
    render layout: 'certificate'

    @name = params[:name]
  end
end
