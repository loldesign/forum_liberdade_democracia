class NewsController < ApplicationController
  def index
    @news = NewsPresenter.map(News.all, view_context)
  end
end
