class SandboxController < ApplicationController

  def index
    render "/sandbox/#{params[:partial_name] || 'index'}"
  end
end
