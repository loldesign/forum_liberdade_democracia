class UsersController < ApplicationController
  inherit_resources

  actions :new, :create

  def create
    create! do |success, failure|
      success.html do
        ContactMailer.send_user_confirmation(@user).deliver if Rails.env.production?

        redirect_to new_user_path, notice: 'Inscrição realizada com sucesso.'
      end
    end
  end

  def confirm_attend
    @user = User.find_by_unique_identifier params[:unique_identifier]

    if @user.update_attribute(:want_to_attend, true)
        redirect_to root_path, notice: 'Sua presença no evento foi confirmada com sucesso.'
    else
      flash[:error] = 'Não possível confirmar sua presença no evento devido a um erro no sistema. Tente mais tarde.'
      redirect_to root_path
    end
  end

  private

  def permitted_params
    params.permit(user: [:name, :phone, :rg, :email, :newsletters, :company])
  end
end
