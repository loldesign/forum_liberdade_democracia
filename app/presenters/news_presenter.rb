class NewsPresenter < ClassicPresenter::Base
  def side(index)
    index.even? ? 'left' : 'right'
  end

  def external_link
    uri = URI(link)
    uri.scheme.nil? ? "http://#{link}" : link
  end
end
