class UserFiltersManager
  def initialize(params)
    @params = params
  end

  def filter
    @users = User.where(created_at: [Date.today.at_beginning_of_year..Date.today.at_end_of_day])
    @users = @users.where(want_to_attend: true) if @params[:by_want_to_attend].present?
    @users = @users.where(attended: true)       if @params[:by_attended].present?
    @users
  end
end